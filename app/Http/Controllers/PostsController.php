<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests\PostsRequest;

use App\Post;

class PostsController extends Controller
{
    public function create(PostsRequest $request)
    {
        if ($request->isMethod('get')) {
            return view('posts.create');
        }

        Post::create([
            'title'   => $request->title,
            'content' => $request->content,
            'user_id' => auth()->id(),
        ]);

        return redirect()->route('listPosts');
    }

    public function list(Request $request)
    {
        $posts = Post::orderBy('id', 'desc')->paginate(5);

        return view('posts.list', compact('posts'));
    }
}
