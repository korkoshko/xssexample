@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="col-12">
            <h1>List posts</h1>
            <div class="list-group">
                @forelse($posts as $post)
                    <a href="#" class="list-group-item list-group-item-action flex-column align-items-start">
                        <div class="d-flex w-100 justify-content-between">
                            <h5 class="mb-1">{{ $post->title }}</h5>
                            <small>{{ $post->created_at }}</small>
                        </div>
                        <p class="mb-1">{!! $post->content !!}.</p>
                    </a>
                @empty
                    <h5>Not found : C</h5>
                @endforelse
            </div>
        </div>
    </div>
@endsection
