@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <h1>Create post</h1>
            <form action="{{ route('createPost') }}" method="post">
                @csrf
                <div class="form-group">
                    <label for="title">Title</label>
                    <input type="text" class="form-control" name="title" />
                </div>

                <div class="form-group">
                    <label for="content">Content</label>
                    <textarea rows="5" class="form-control" name="content"></textarea>
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-primary">
                        Create
                    </button>
                    <button type="reset" class="btn btn-default">
                        Cancel
                    </button>
                </div>
            </form>
        </div>

    </div>
</div>
@endsection
