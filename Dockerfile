FROM php:7.3

RUN apt-get update
# Core
RUN apt-get install -y unzip git
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer
# Debug
RUN docker-php-ext-install pdo_mysql mysqli

WORKDIR /app
